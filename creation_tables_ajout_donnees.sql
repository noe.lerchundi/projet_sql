drop table if exists Resultats;
drop table if exists Candidats;
drop table if exists Circonscriptions;
drop table if exists Communes;
drop table if exists Departements;
drop table if exists Bureaux;

CREATE TABLE Candidats(
	num_panneau integer primary key not null,
	sexe text not null,
	nom_candidat text not null,
	prenom_candidat text not null
);

create table Bureaux(
	code_insee int not null,
	bureau_de_vote int not null,
	nom_b text,
	adresse_b text,
	code_postal_b int,
	ville_b text,
	inscrits_b int,
	latitude_b text,
	longitude_b text,
	abstentions_b int,
	votants_b numeric,
	exprimes_b numeric,
	nuls_b numeric,
	blancs_b numeric,
	p_abst_ins_b numeric,
	p_nuls_vot_b numeric,
	p_nuls_ins_b numeric,
	p_vote_ins_b numeric,
	p_blancs_ins_b numeric,
	p_blancs_vot_b numeric,
	p_expr_ins_b numeric,
	p_expr_vot_b numeric,
	p_voix_ins_b numeric,
	p_voix_expr_b numeric,
	primary key (code_insee, bureau_de_vote)
);

create table Départements (
	code_département int primary key not null,
	nom_département text not null
);

create table Circonscriptions (
	numéro_cir int not null,
	code_département int not null,
	nom_cir text,
	constraint fk_code_département
		foreign key(code_département)
		references Départements(code_département),
	primary key(numéro_cir, code_département)
);

create table Communes (
	code_commune int not null,
	code_département int not null,
	nom_commune text,
	nb_bureaux int,
	constraint fk_code_département
		foreign key(code_département)
		references Départements(code_département),
	primary key(code_commune, code_département)
);

create table Résultats (
	voix_récoltées numeric not null,
	num_panneau int not null,
	code_insee int not null,
	bureau_de_vote int not null,
	constraint fk_num_panneau
		foreign key(num_panneau)
		references Candidats(num_panneau),
	constraint fk_code_insee
		foreign key(code_insee)
		references Bureaux(code_insee),
	constraint fk_bureau_de_vote
		foreign key(bureau_de_vote)
		references Bureaux(bureau_de_vote),
	primary key (num_panneau, code_insee, bureau_de_vote)
);


insert into Candidats 
select distinct "N°Panneau" , "Sexe" , "Nom" , "Prénom" 
from "election-csv" ec;

insert into Bureaux
select distinct "Code Insee", "Bureau de vote", "Nom Bureau Vote", "Adresse", "Code Postal", "Ville", "Inscrits",
	   split_part("Coordonnées"::text,',', 1),
	   split_part("Coordonnées"::text,',', 2),
	   "Abstentions", "Votants", "Exprimés", "Nuls", "Blancs", "% Abs/Ins", "% Nuls/Ins", "% Nuls/Ins", 
	   "% Vot/Ins", "% Blancs/Ins", "% Blancs/Vot", "% Exp/Ins", "% Exp/Vot", "% Voix/Ins", "% Voix/Exp"
from "election-csv" ec2 ;


insert into Départements
select distinct "Code du département" , "Département" from "election-csv" ec;

select split_part("Coordonnées"::text,',', 1) as _numeric,
	   split_part("Coordonnées"::text,',', 2) as _numeric
	   from "election-csv" ec ;

select "Coordonnées" from "election-csv" ec limit 1;