drop table if exists Resultats;
drop table if exists Appartient;
drop table if exists Candidats;
drop table if exists Bureaux;
drop table if exists Circonscriptions;
drop table if exists Communes;
drop table if exists Departements;

CREATE TABLE Candidats(
	num_panneau integer primary key not null,
	sexe text not null,
	nom_candidat text not null,
	prenom_candidat text not null
);

create table Departements (
	code_departement int primary key not null,
	nom_departement text not null
);

create table Communes (
	code_commune int not null,
	code_departement int not null,
	nom_commune text,
	constraint fk_code_departement
		foreign key(code_departement)
		references Departements(code_departement),
	primary key(code_commune, code_departement)
);

create table Circonscriptions (
	code_circonscription int not null,
	code_departement int not null,
	nom_cir text,
	constraint fk_code_departement
		foreign key(code_departement)
		references Departements(code_departement),
	primary key(code_circonscription, code_departement)
);

create table Bureaux(
	code_insee int not null,
	bureau_de_vote int not null,
	nom_b text,
	adresse_b text,
	code_postal_b int,
	ville_b text,
	inscrits_b int,
	latitude_b text,
	longitude_b text,
	abstentions_b int,
	votants_b numeric,
	exprimes_b numeric,
	nuls_b numeric,
	blancs_b numeric,
	code_commune_b int not null,
	code_departement_b int not null,
	constraint fk_com
		foreign key (code_commune_b,code_departement_b) references Communes(code_commune, code_departement),
	primary key (code_insee, bureau_de_vote)
);

create table Appartient (
	code_insee int not null,
	bureau_de_vote int not null,
	code_circonscription int not null,
	code_departement int not null,
	constraint fk_code_insee
		foreign key(code_insee, bureau_de_vote)
		references Bureaux(code_insee, bureau_de_vote),
	constraint fk_code_circonscription
		foreign key(code_circonscription, code_departement)
		references Circonscriptions(code_circonscription, code_departement),
	primary key (code_insee, bureau_de_vote, code_circonscription, code_departement)
)


create table Resultats (
	voix_recoltees numeric not null,
	num_panneau int not null,
	code_insee int not null,
	bureau_de_vote int not null,
	constraint fk_num_panneau
		foreign key(num_panneau)
		references Candidats(num_panneau),
	constraint fk_code_insee
		foreign key(code_insee, bureau_de_vote)
		references Bureaux(code_insee, bureau_de_vote),
	primary key (num_panneau, code_insee, bureau_de_vote)
);

--
--
--  Insertion des donnees dans les tables.
--
--


insert into Candidats 
select distinct "N°Panneau" , "Sexe" , "Nom" , "Prénom" 
from "election-csv" ec;

insert into Departements
	select distinct "Code du département" , "Département" 
	from "election-csv" ec;
	
insert into Circonscriptions
	select distinct "Code de la circonscription" , "Code du département" , "Circonscription" 
	from "election-csv" ec ;

insert into Communes
	select distinct "Code de la commune", "Code du département", "Commune" 
	from "election-csv" ec ;


insert into Bureaux
	select distinct "Code Insee", "Bureau de vote", 
	   "Nom Bureau Vote", "Adresse", "Code Postal", "Ville", "Inscrits",
	   split_part("Coordonnées"::text,',', 1),
	   split_part("Coordonnées"::text,',', 2),
	   "Abstentions", "Votants", "Exprimés", "Nuls", "Blancs", "Code de la commune","Code du département"
	from "election-csv" ec2 ;

insert into Appartient
	select distinct "Code Insee", "Bureau de vote" , "Code de la circonscription" , "Code du département" 
	from "election-csv" ec ;

insert into Resultats
	select "Voix", "N°Panneau", "Code Insee" , "Bureau de vote" from "election-csv" ec ;

	
--
--
--  Pour chaque ville, pour chaque candidat : donnez le résultat du candidat (%) ainsi que son classement.
--



select (b2.code_commune_b,b2.code_departement_b), r.num_panneau, r.voix_recoltees, b2.exprimes_b ,
	sum(r.voix_recoltees)/(
		select b2.exprimes_b
		from bureaux b2 
			join resultats r on (b2.code_insee, b2.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
			join communes c2 on (b2.code_commune_b,b2.code_departement_b) =(c2.code_commune,c2.code_departement )
		group by (b2.code_commune_b,b2.code_departement_b), b2.exprimes_b 
	)::real as "%_candidat"	
	from bureaux b2 
		join resultats r on (b2.code_insee, b2.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
		join candidats c on c.num_panneau = r.num_panneau
		join communes co on (co.code_commune,co.code_departement) = (b2.code_commune_b,b2.co
	group by (b2.code_commune_b, b2.code_departement_b),r.num_panneau,r.voix_recoltees ,b2.exprimes_b 
	order by (b2.code_commune_b,b2.code_departement_b), r.num_panneau;


--280 Communes
select c2.nom_commune from Communes c2 group by c2.nom_commune;

--donc pour chaque communes nous aurons besoin de 12*280 %voix_recoltee


---il fau tle nb de voix recoltees par candidat par commune

select c2.nom_commune, sum(r.voix_recoltees) 
	from resultats r 
	join bureaux b2 on (b2.code_insee, b2.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
	join communes c2 on b2.code_commune_b=c2.code_commune 
	group by r.num_panneau,c2.nom_commune;

--il faut le nb de voix exprime par commune

select c2.nom_commune, sum(b2.exprimes_b)
	from Communes c2 
	join bureaux b2 on b2.code_commune_b=c2.code_commune 
	group by c2.nom_commune;



select r.num_panneau , c.nom_commune, 
	100*sum(r.voix_recoltees)/(
	select sum(b2.exprimes_b)
	from Communes c2 
		join bureaux b2 on b2.code_commune_b=c2.code_commune 
		where c2.nom_commune LIKE '%Lyon%'
		group by c2.nom_commune
		order by c2.nom_commune
	)::float as "%_candidat"
	from resultats r 
	join bureaux b on (b.code_insee, b.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
	join communes c on b.code_commune_b=c.code_commune 
	group by r.num_panneau,c.nom_commune
	order by c.nom_commune;

--227 voix exprimée /227 voix recoltees


select r.num_panneau , c.nom_commune, 
	100*sum(r.voix_recoltees)/(
	select sum(b.exprimes_b)
	from Communes c 
		join bureaux b on (b.code_commune_b,b.code_departement_b) =(c.code_commune,c.code_departement)
		where b.code_departement_b <> c.code_departement
		group by c.nom_commune
		order by c.nom_commune
	)::real as "%_candidat"
	from resultats r 
	join bureaux b on (b.code_insee, b.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
	join communes c on (b.code_commune_b,b.code_departement_b) =(c.code_commune,c.code_departement)
	group by r.num_panneau,c.nom_commune
	order by c.nom_commune;





select r.num_panneau , c2.nom_commune, 
	sum(r.voix_recoltees)/(
	select sum(b.exprimes_b)
	from Communes c 
		join bureaux b on (b.code_commune_b,b.code_departement_b) =(c.code_commune,c.code_departement)
		where c.nom_commune='Lyon'
		group by c.nom_commune
	)::real as "%_candidat"
	from resultats r 
	join bureaux b on (b.code_insee, b.bureau_de_vote) = (r.code_insee, r.bureau_de_vote)
	join communes c2 on (b.code_commune_b,b.code_departement_b) =(c2.code_commune,c2.code_departement)
	where c2.nom_commune='Lyon'
	group by r.num_panneau,c2.nom_commune
	order by c2.nom_commune;
